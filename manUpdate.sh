#!/bin/bash

askAlways=false
cancelHit=false
version="v0.3.0"

# preCheck
neededApp="dialog"
aptAppsList=("$neededApp:false" "flatpak:false" "snapd:false")

checkApps()
{
	appCounter=0
	printf "\n--> Überprüfung von notwendigen Paketen\n"

	for appCreds in "${aptAppsList[@]}"; do

		# printf "\nappCreds='$appCreds'"
		appName="${appCreds%%:*}"
		appFound="${appCreds##*:}"
		printf "\nÜberprüfe '$appName' ($appCounter)\t.. ist"

		appFound=$(dpkg-query -W -f='${Status}' "$appName" 2>/dev/null | grep -c "ok installed")
		if [ $appFound -eq 1 ]; then
			aptAppsList[$appCounter]="$appName:true"
			printf " installiert!"

		else
			printf " nicht installiert!"
			
			if [ "$appName" == "$neededApp" ]; then
				printf "\n\t .. installiere '$neededApp' via 'sudo apt update && sudo apt install $neededApp'\n"
				sudo apt update && sudo apt install "$neededApp"
				aptAppsList[$appCounter]="$appName:true"
			fi
		fi
		let appCounter++
	done
	sleep 2s
}

cleanSnaps()
{
	snapList=($(snap list --all | awk '/disabled/{print $1, $3}' | tr ' ' '_'))
	snapItems="${#snapList[@]}"
	# echo "snapItems='$snapItems'"

	if [ $snapItems -gt 0 ]; then
		echo "entferne $snapItems alte Snaps .."

		for value in "${snapList[@]}"; do
			snapInfo=($(echo "$value" | tr '_' ' '| awk '{print $1, $2}'))
			sudo snap remove "${snapInfo[0]}" --revision="${snapInfo[1]}"
		done

	else
		echo "No old snaps found!"
	fi

	echo "lösche snaps-cache -> '/var/lib/snapd/cache/' .."

	sudo du -sh /var/lib/snapd/cache/
	sudo find /var/lib/snapd/cache/ -exec rm -v {} \;
}

askUser()
{
	runCmd=true

	if [ $askAlways = true ]; then
		response=""
		echo ""
		read -r -p "Ausführen von '$1'? (j/n ENTER-Taste=n)" response

		case $response in
		    j|J) runCmd=true ;;
		    *) runCmd=false ;;
		esac
	fi
	
	if [ $runCmd == true ]; then printf "\n--> starte '$1'\n" && sleep 1s && $1
	else printf "\n--> überspringe '$1'\n" && sleep 1s; fi
}

checkApps
RESULT=$(dialog --title "manUpdate.sh $version" --menu "Dieses script wird folgende ’apt-get’-Kommandos ausführen\nupdate, upgrade, dist-upgrade, autoremove, autoclean\n(in dieser Reihenfolge)\n\nBitte Wählen Sie eine Option:" 15 70 16 1 "genau so fortfahren" 2 "fortfahren, aber vor jedem Kommando fragen" 3 "abbrechen" 3>&1 1>&2 2>&3 3>&-);

clear

case $RESULT in
	1) printf "\nüberspringe keine der Kommandos.\n" ;;
	2) askAlways=true ;;
	*) printf "\nbreche Vorgang ab.\n" && cancelHit=true ;;
esac

if [ $cancelHit == false ]; then
	printf "\n*Update/Upgrade*"
	askUser "sudo apt update"
	askUser "sudo apt upgrade"
	askUser "sudo apt dist-upgrade"

	printf "\n*Aufräumen*"
	askUser "sudo apt autoremove"
	askUser "sudo apt autoclean"

	if [ "${aptAppsList[1]}" == "flatpak:true" ]; then
		printf "\n-->Update flatpak"
		askUser "flatpak update"

		printf "\n*Entfernen ungenutzter flatpak-Pakete*"
		askUser "flatpak uninstall --unused"

		printf "\n*Aufräumen des flatpak-cache*"
		askUser "sudo rm -rfv /var/tmp/flatpak-cache-*"
	fi

	if [ "${aptAppsList[2]}" == "snapd:true" ]; then
		printf "\n-->Update 'snap'"
		askUser "sudo snap refresh"

		printf "\n*Entfernen alter snaps*"
		cleanSnaps
	fi

	printf "\n*Update Grub/initramfs*"

	## note: for multiple or alternate bootloaders installed
	# askUser "sudo update-bootloaders"
	# askUser "sudo burg-install /dev/sda"

	## regular grub
	askUser "sudo update-grub"

	## note: use if you want to install grub from the current system
	# askUser "sudo grub-install /dev/sda"

	askUser "sudo update-initramfs -u"
fi

printf "\nmanUpdate.sh $version --> fertig. :)"
sleep 1s && exit 0
