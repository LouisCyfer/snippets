#!/bin/bash
cd $(dirname "$0")
wDir=$(pwd)
myVersion="v0.0.1"

# echo "wDir=$wDir/"$(basename "$0")
appName="TelegramDesktop"
dfName="/usr/share/applications/telegram.desktop"

# for regular usage without using this updater script
appCmd="telegram %u"

echo -e "\n[$appName updater] Welcome!\n\tThis script contains following features:"
echo -e "\n\t- checking for github-releases of $appName"
echo -e "\t--> https://telegram.org/dl/desktop/linux"
echo -e "\t- taking care of atareao-releases and it's local sources"
echo -e "\t- updating if necessary or a new version is found"
echo -e "\t- 'releases.txt' contains the current installed version number."
echo -e "\t- if 'releases.txt' gets lost or deleted then it will update anyways."
echo -e "\t- creating a starter file which is capable of running this script"
echo -e "\n\tThank you for using this script!"

sleep 2s

# for the updater .desktop file
updCmd="gnome-terminal --geometry 85x10+10+120 -e '$wDir/"$(basename "$0")"'"
echo -e "\n[$appName updater] .. checking for latest release"

checkNetwork() {
	echo -e "\n[$appName updater] checking for internet connectivity"
	ipCheck=$(curl -s --connect-timeout 1 --max-time 1 "https://canhazip.com")

	online="false"
	if [ -n "$ipCheck" ]; then online="true"; fi
	echo "ipCheck $online $ipCheck"
}

online=$(checkNetwork)
online=$(echo "$online" | grep "ipCheck" | cut -d " " -f 2)
echo -e "\tonline='$online'"
if [ "$online" == "false" ]; then echo "\tYou might want to check your internet connection!"; exit 1; fi

# taking care of atareao's outdated launchpad.net repository
# https://launchpad.net/~atareao/+archive/ubuntu/telegram

remove=false
dQuery=$(dpkg-query -W -f='${Status} ${Version}\n' "telegram")
tCheck=$(echo "$dQuery" | cut -d " " -f 3)
# echo "tCheck=$tCheck"

if [ "$tCheck" == "installed" ]; then
	tVersion=$(echo "$dQuery" | cut -d " " -f 4)
	echo -e "\n\t'$tVersion' found installed. Usually it's perfectly safe if unstalled & reinstalled\n\tunless you delete '.local/share/telegram'\n"

	response=$(zenity --question --title="[$appName updater]" --text="'$tVersion' found installed.\n\nDo you wish to unstall it?" 2>/dev/null; echo $?)
	# echo -e "response='$response'"

	if [ "$response" -eq 0 ]; then
		echo "executing 'sudo apt remove telegram'"
		sudo apt remove telegram
	fi

	response=$(zenity --question --title="[$appName updater]" --text="Do you wish to clear out all 'atareao' sources from\n'/etc/apt/sources.list.d/'?" 2>/dev/null; echo $?)
	# echo -e "response='$response'"

	if [ "$response" -eq 0 ]; then
		echo "scanning and removing all 'atareao' sources from '/etc/apt/sources.list.d/'"
		atSources=($(ls -tda1 -- /etc/apt/sources.list.d/* | grep atareao))

		for item in "${atSources[@]}"; do
			echo -e "\tremoving '"$(basename "$item")"'"
			sudo rm "$item"
		done
	fi
fi

updateNeeded=false

# grabbing indirectly via https://telegram.org/dl/desktop/linux, reading version from package
latestLink=("https://telegram.org/dl/desktop/linux")
latestLink=$(wget -nv --content-disposition --spider "$latestLink" 2>&1 | cut -d " " -f 4 | tr "\n" " " | cut -d " " -f 2)
latestVersion="v"$(basename "$latestLink" | rev | cut -d "." -f 3- | rev | cut -d "." -f 2-)

currVer=""
if [ -f "./releases.txt" ]; then currVer=$(cat ./releases.txt | jq -r '.currVer'); fi

if [ "$currVer" != "$latestVersion" ]; then updateNeeded=true; fi
echo -e "\n\tlatestLink='$latestLink'\n\tcurrVer='$currVer' latestVersion='$latestVersion' updateNeeded=$updateNeeded"

if [ $updateNeeded == true ]; then

	# download latest version
	packageName=$(basename "$latestLink")
	echo -e "\n[$appName updater] updating Telegram to $latestVersion\n\tgrabbing $packageName via wget\n\trelease link --> $latestLink"
	wget -q --show-progress "$latestLink"

	echo -e "\n\tunpacking via sudo to /opt/"
	sudo tar -v -Jxf "$packageName" -C /opt/ && rm "$packageName"

	sysLink="/usr/local/bin/telegram"
	echo -e "\n\tupdating sysLink ($sysLink)"
	
	if [ -f "$sysLink" ]; then sudo rm "$sysLink"; fi
	sudo ln -s /opt/Telegram/Telegram "$sysLink"

	# change/update .desktop file
	# Exec=/opt/telegram/Telegram -- %u
	# Icon=/opt/telegram/telegram.svg

	echo -e "\tupdating $dfName\n"

	dFile="[Desktop Entry]\nEncoding=UTF-8\nVersion=1.0\nName=Telegram Desktop\nComment=Official desktop version of Telegram messaging app"
	dFile+="\nExec=$appCmd\nIcon=telegram"
	dFile+="\nTerminal=false\nStartupWMClass=Telegram\nType=Application\nCategories=GNOME;GTK;Network;"
	dFile+="\nMimeType=application/x-xdg-protocol-tg;x-scheme-handler/tg;\nX-Desktop-File-Install-Version=0.22"

	# echo -e "dFile=\n$dFile"

	if [ -f "$dfName" ]; then
		dFile=$(cat "$dfName")
		dFile=$(echo -e "$dFile" | sed '/Exec=/ c\'"Exec=$appCmd") # /opt/Telegram/Telegram -- %u
		dFile=$(echo -e "$dFile" | sed '/Icon=/ c\'"Icon=telegram")
	fi

	# echo -e "\tdFile=\n$dFile"
	if [ -f "$dfName" ]; then sudo rm "$dfName"; fi
	echo -e "$dFile" | sudo tee --append "$dfName"

	dufName="./telegramUpdater.desktop"

	echo -e "\tcreating $dufName\n"

	dFile="[Desktop Entry]\nName=TelegramDesktop Updater\nExec=$updCmd\nIcon=telegram"
	dFile+="\nTerminal=false\nType=Application"

	# dufName="./"$(basename "$dfName")
	echo -e "$dFile" > "$dufName" && chmod +x "$dufName"

	currVer="$latestVersion"
	echo -e "\n[$appName updater] updating releases.txt"
	jq -n '{currVer: "'"$currVer"'"}' > releases.txt

	echo -e "\nsuccess!! :)"
fi

sleep 2s && exit 0

