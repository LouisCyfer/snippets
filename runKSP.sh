#!/bin/bash

preCmd=""

KSPpath=""
prePath=$(dirname "$0")"/" # "/mnt/Games/LinuxInstalledGames/KSP/"

cd $prePath

allPathes=$(ls -d "KerbalSpaceProgram_"*/ | sort -r -h | tr -d '/')

Kpaths=("$0")
Kpaths+=("KSP_CKAN")
Kpaths+=($(echo "$allPathes"))

maxPathes=${#Kpaths[@]}

idx=0
while [ $idx -lt $maxPathes ]; do
	echo "Kpaths[$idx]=${Kpaths[$idx]}"
	let idx=idx+1
done

# sleep 10s && exit 0

KSPapp="KSP.x86"
found64bit=false

if [ "$(uname -m)" = "x86_64" ]; then
	KSPapp=$KSPapp"_64"
	found64bit=true
fi

#possible params --> -force-opengl -force-opengl -force-gfx-direct
kspParams=" -force-glcore" # -force-glcoreXY --> accepted values for XY: 30, 31 or 32.

# --> forcing to set other cpu cores (for me it was not quite performance improving) 
# another FSP alternative --> glxosd

# change resolution in settings.cfg
changeRes=true
defRes=0
res=("1877 1028 compiz") # compiz
res+=("1885 1018 gnome_alt") # gnome alt
res+=("1920 1016 gnome") # gnome
maxRes=${#res[@]}


# resW="SCREEN_RESOLUTION_WIDTH = "$(echo ${res[$defRes]} | cut -d ' ' -f1)
# resH="SCREEN_RESOLUTION_HEIGHT = "$(echo ${res[$defRes]} | cut -d ' ' -f2)

# maxFPS=30
KSPsettings=("SCREEN_RESOLUTION_WIDTH = "$(echo ${res[$defRes]} | cut -d ' ' -f1))
KSPsettings+=("SCREEN_RESOLUTION_HEIGHT = "$(echo ${res[$defRes]} | cut -d ' ' -f2))
KSPsettings+=("FRAMERATE_LIMIT = 30")
KSPsettings+=("SHOW_SPACE_CENTER_CREW = False")

# set to use all 4 CPU cores
preRun="taskset -c 0-3"


goAhead=true
runCKAN=false
editMe=false

optionID=0
kspPID=0

clear

info=""

msg="32bit"
if [ "$found64bit" = true ]; then msg="64bit"; fi
echo "NOTE: found $msg OS $(uname -s), using '$KSPapp$kspParams' if you choose to start KSP"

optionID=0
options=()
optionsArray=()

# --> old method
#setTrue=$(($maxPathes - 1)) # workaround
#setTrue=1

# setTrue="KerbalSpaceProgram_1.4.2"
setTrue="KerbalSpaceProgram_current"

idx=0
# ${#Kpaths[@]}

while [ $idx -lt $maxPathes ]; do

	case $idx in
		0)	options[$idx]="edit this script | run gedit '${Kpaths[$idx]}'" ;;
		1)	options[$idx]="run mono 'ckan.exe' from ./${Kpaths[$idx]}/" ;;
		*)	options[$idx]="run ./${Kpaths[$idx]}/$KSPapp$kspParams" ;;
	esac

	# if [ $idx = 1 ]; then options[$idx]="run mono 'ckan.exe' from ./${Kpaths[$idx]}/";
	# else options[$idx]="run ./${Kpaths[$idx]}/$KSPapp$kspParams"; fi

	setTo="FALSE"
	if [ "$setTrue" = "${Kpaths[$idx]}" ]; then setTo="TRUE"; fi
	# if [ $idx -eq ${setTrue[1]} ]; then setTo="TRUE"; fi

	optionsArray+=("$setTo" "${options[$idx]}")

	let idx=idx+1
	# idx=$(($idx + 1))
done

echo -e "\n[:: KSP Launcher | pick path ::]\n--> Wich folder you wish to use?"

response=$(zenity --title="[:: KSP Launcher | pick path ::]" --width=600 --height=350 --list --radiolist --text="$info" \
--column="" --column="Wich application & folder you wish to use?" "${optionsArray[@]}")
# FALSE "${options[0]}" TRUE "${options[1]}" FALSE "${options[2]}" FALSE "${options[3]}" FALSE "${options[4]}" FALSE "${options[5]}" FALSE "${options[6]}")

idx=0
while [ $idx -lt ${#options[@]} ]; do

	if [ "${options[$idx]}" == "$response" ]; then
		let optionID=idx+1
		break
	fi

	let idx=idx+1
	# idx=$(($idx + 1))
done

if [ $optionID = 0 ]; then
	goAhead=false

else
	goAhead=true
	changeRes=false

	KSPpath=$prePath${Kpaths[$(($optionID - 1))]}

	case $optionID in
		1)	editMe=true ;;
		2)	runCKAN=true ;;
		*)	changeRes=true ;;
	esac

	if [ $changeRes = true ]; then
		echo -e "using path '$KSPpath'\n"

		# get compiz or gnome-shell PID
		setTrue=1 # preset id

		compizPID=$(pidof "compiz")
		gsPID=$(pidof "gnome-shell")

		echo -e "compizPID:$compizPID\ngsPID:$gsPID"

		if [ -n "$gsPID" ]; then setTrue=1; fi
		if [ -n "$compizPID" ]; then setTrue=0; fi
	
		optionsArray=()
		options=()
		idx=0

		while [ $idx -lt $maxRes ]; do
			oStr=$(echo ${res[$idx]} | cut -d ' ' -f1)" x "$(echo ${res[$idx]} | cut -d ' ' -f2)" (note: "$(echo ${res[$idx]} | cut -d ' ' -f3)")"

			options[$idx]="set resolution to $oStr"
			setTo="FALSE"
			if [ $idx -eq $setTrue ]; then setTo="TRUE"; fi

			optionsArray+=("$setTo" "${options[$idx]}")

			let idx=idx+1
			# idx=$(($idx + 1))
		done

		response=$(zenity --title="[:: KSP Launcher | pick resolution ::]" \
		--width=600 --height=350 --list --radiolist --text="$info" \
		--column="" --column="Wich resolution you wish to use?" "${optionsArray[@]}")

		idx=0
		optionID=$defRes

		while [ $idx -lt ${#options[@]} ]; do

			if [ "${options[$idx]}" == "$response" ]; then
				let optionID=idx+1
				break
			fi

			let idx=idx+1
			# idx=$(($idx + 1))
		done

		if [ $optionID = 0 ]; then
			goAhead=false
		else

			width=$(echo ${res[$idx]} | cut -d ' ' -f1)
			height=$(echo ${res[$idx]} | cut -d ' ' -f2)

			# width=1920 # 1877
			# height=1016 # 1028

			cd "$KSPpath"
			echo -e "\nwriting 'settings_backup.cfg'\n"
			cp "./settings.cfg" "./settings_backup.txt"

			# workaround
			#prefsFile="$HOME/.config/unity3d/Squad/Kerbal Space Program/prefs"

			#if [ -e "$prefsFile" ]; then
				#echo "removing '$prefsFile'" && sleep 0.2s
				#rm "$prefsFile"
			#fi

			# ${KSPsettings[0]}

			settingsFile=$(cat "./settings.cfg")

			KSPsettings[0]="SCREEN_RESOLUTION_WIDTH = $width"
			KSPsettings[1]="SCREEN_RESOLUTION_HEIGHT = $height"

			echo "set resolution to $width x $height in 'settings.cfg'"
			settingsFile=$(echo -e "$settingsFile" | sed '/SCREEN_RESOLUTION_WIDTH/ c\'"${KSPsettings[0]}")
			settingsFile=$(echo -e "$settingsFile" | sed '/SCREEN_RESOLUTION_HEIGHT/ c\'"${KSPsettings[1]}")

			# DEBUG for new resolution entries
			echo -e "\t"$(echo -e "$settingsFile" | grep "SCREEN_RESOLUTION_WIDTH")
			echo -e "\t"$(echo -e "$settingsFile" | grep "SCREEN_RESOLUTION_HEIGHT")"\n"

			echo "setting ${KSPsettings[2]} in 'settings.cfg'"
			settingsFile=$(echo -e "$settingsFile" | sed '/FRAMERATE_LIMIT/ c\'"${KSPsettings[2]}")

			# DEBUG for new FPS entry
			echo -e "\t"$(echo -e "$settingsFile" | grep "FRAMERATE_LIMIT")"\n"

			echo "setting ${KSPsettings[3]} in 'settings.cfg'"
			settingsFile=$(echo -e "$settingsFile" | sed '/SHOW_SPACE_CENTER_CREW/ c\'"${KSPsettings[3]}")

			echo -e "$settingsFile" > "./settings.cfg"
			echo "done, proceeding .."
		fi
	fi

	if [ $goAhead = true ]; then

		if [ $editMe = true ]; then
			echo -e "editMe=$editMe"
			gedit "$0"

		elif [ $runCKAN = true ]; then
			echo -e "\n[:: starting ckan.exe (Mono) ::]"
	    		cd "$KSPpath" && mono 'ckan.exe' && cd

		else
			echo -e "\n[:: starting KSP ::]\n"
			cd "$KSPpath"

			echo 'running --> export LC_ALL=C'
			export LC_ALL=C

			echo 'running --> export LD_PRELOAD="libpthread.so.0 libGL.so.1"'
			export LD_PRELOAD="libpthread.so.0 libGL.so.1"

			# echo 'running --> export __GL_THREADED_OPTIMIZATIONS=1'
			# export __GL_THREADED_OPTIMIZATIONS=1

			echo "executing > $preRun./$KSPapp$kspParams &"
			sleep 2s
			exec $preRun ./$KSPapp$kspParams &

			echo ""
			sleep 1s

			kspPID=$(pidof "$KSPapp")
			echo "$kspPID"

			if [ -n "$kspPID" ]; then
				echo -e "starting logger:\n----------------"
				tail -f "$KSPpath/KSP.log" &

				while :
				do
					kspPID=$(pidof "$KSPapp")

					if [ -n "$kspPID" ]; then
						sleep 1s
					else
						echo "$KSPapp has closed"
						break
					fi
				done
			fi
		fi
	fi
fi

if [ $goAhead = false ]; then
	echo "You've cancelled starting-up."
	zenity --info --text="You've cancelled starting-up."
fi

echo ""
echo "done. :)"
sleep 5s

exit 0