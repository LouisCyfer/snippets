## snippets
> * en: this repo contains some useful scripts and code-snippets and is yet work in progress
> * de: Dieses Repo enthält einige Skripte und Code-Schnipsel und wird noch erweitert

- [x] This repo is still WIP!
- [x] MIT licensed
- [x] feel free to use and/or mod this snippets
- [x] have fun!

## FSupdater.sh
intends to update the Linux version of the Firestorm Viewer automagicly
>Firestorm is a SecondLife™ 3rd party viewer and can be found here: https://www.firestormviewer.org

## SLupdater.sh
intends to update the Linux version of the SecondLife™ Viewer automagicly
>The official SecondLife™ Viewer can be found here: https://secondlife.com/support/downloads/

## mUpdater.sh
small TelegramDesktop client update script (Ubuntu)
- checking for github-releases of TelegramDesktop --> https://github.com/telegramdesktop/tdesktop/releases/latest
- taking care of atareao-releases and it's local sources
- updating if necessary or a new version is found
- 'releases.txt' contains the current installed version number. (if lost or deleted then it will update anyways)
- creating a starter file which is capable of running this script

## manUpdate.sh
- update-script for Ubuntu-like distro's (14/16/18/20.04, use with caution on other distro's)
- might become multilingual in future
- needs the package 'dialog'

## runKSP.sh
small startup script for KSP under Linux :D

Install this script next to your KSP install folders like this:
* ./ANY_FOLDER
    * this script
    * ./KerbalSpaceProgram_1.4.1
    * ./KerbalSpaceProgram_1.4.0
    * ./KerbalSpaceProgram_test
    * ./KerbalSpaceProgram_debug

> * Every install folder must start with "KerbalSpaceProgram_" or the script can't recognise this folder as KSP install and will not list it as such.
> * Also an option has been included to edit the script itself via gedit (most likely installed on every debian release) and start CKAN.exe via mono.

## RAMcleaner.sh
will clean your RAM (basicly unloads system components off the RAM)

>CAUTION! this script is intended to be used in conky!

* set the max lowest RAM usage in the script (i assume 25% is just fair enough, otherwise mod that)
* set ask to false if you wish not to get bothered by clicking yes/no

example usage in any conky script:
```
${execi 60 ~/.conky/RAMcleaner.sh}
```

