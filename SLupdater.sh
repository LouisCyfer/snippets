#!/bin/bash

# general values
ownVersion="0.0.0.2"
scriptName=$(basename "$0")

#arch=$(uname -m) # only works for i386 or x86_64!
arch="i686"
cd $(dirname "$0")
wDir=$(pwd)

appName="SecondLife Viewer"
echo -ne "\033]0;[:: $appName Updater - version $ownVersion ::]\007"

appCMD="./secondlife"
forcedUpdate=false

echo -e "\n\n\n[$appName updater] checking releases\n\tarch:\t'$arch'\n\twDir:\t'$wDir'\n"

userSettingsDir="$HOME/.secondlife"

excludeItems=("SLupdater.sh" ".backups")

# custom exclude folders, you might want to preconfigure yourself
# in Firestorm and/or this line:
##excludeItems+=("BACKUPS" "CACHE" "CHATLOGS")

if [ ${#excludeItems[@]} -gt 0 ]; then
	echo -e "\texcluded items from deleting when doing backups:"
	for item in "${excludeItems[@]}"; do
		msgStr="\t\t./$item"
		if [ ! -d "$item" ] && [ ! -f "$item" ]; then msgStr+=" (not found)"; fi
		echo -e "$msgStr"
	done
fi

# lparsing *.json files if available to find any build info
currentVersion=""

usingFallback=true
buildFile="summary.json" # fallback

jsonFiles=($(find . -mindepth 1 -maxdepth 1 -type f -name "*.json" | sort))
echo -e "\n\tfound local *.json files:"

idx=0
for jFile in "${jsonFiles[@]}"; do
	pMsg="\t\t$jFile"

	currentVersion=$(cat "$jFile" | jq -r '.Version')
	pMsg+=" currentVersion='$currentVersion'"

	if [ -n "$currentVersion" ]; then
		pMsg+=" (assumed build info file)"

		buildFile="$jFile"
		usingFallback=false
	fi

	echo -e "$pMsg"
	let idx++
done

echo -e "\n\tbuildFile='$buildFile' usingFallback='$usingFallback'"

# grab installed version & name
currentName=""

if [ -f "$buildFile" ]; then
	currentVersion=$(cat "$buildFile" | jq -r '.Version')
	currentName=$(cat "$buildFile" | jq -r '.Channel')

	appName="$currentName"
	echo -ne "\033]0;[:: $appName Updater ::]\007"
fi
# echo -e "\n\tcurrent version:\t'$currentVersion'"

checkNetwork() {

	echo -e "\n[$appName updater] checking for internet connectivity"
	ipCheck=$(curl -s --connect-timeout 2 --max-time 2 'https://myip.is')

	online="false"
	if [ -n "$ipCheck" ]; then online="true"; fi
	echo "ipCheck $online $ipCheck"
}

online=$(checkNetwork)
online=$(echo "$online" | grep "ipCheck" | cut -d " " -f 2)
if [ "$online" == "false" ]; then echo -e "\tYou might want to check your internet connection!"; exit 1; fi

# grab latest versions & self update
src_url="https://gitlab.com/LouisCyfer/snippets/-/raw/master/$scriptName"
src_self=$(curl -sL "$src_url")
src_version="$ownVersion"

if [ -n "$src_self" ]; then

	nfPattern="Page Not Found"
	checkNotFound=$(echo "$src_self" | grep "$nfPattern")

	if [ "$checkNotFound" != "$nfPattern" ]; then
		src_version=$(echo "$src_self" | grep 'ownVersion' | tr -d '\n' | cut -d '"' -f 2)
	else echo -e "\tcheckNotFound:\t'$checkNotFound'"
	fi
fi

echo -e "\n\t$scriptName versions ($src_url)\n\tsrc_version:\t'$src_version'"
echo -e "\townVersion:\t'$ownVersion'"

versionsEqual=true

# only update if the online-version is higher
if [ "$src_version" != "$ownVersion" ]; then
	# checking for major diffs
	verStep_src=$(echo "$src_version" | cut -d '.' -f 1)
	verStep_own=$(echo "$ownVersion" | cut -d '.' -f 1)
	# echo -e "major\nverStep_src='$verStep_src'\nverStep_own='$verStep_own'\n"

	if [ "$verStep_own" -lt "$verStep_src" ]; then versionsEqual=false
	else
		# checking for minor diffs
		verStep_src=$(echo "$src_version" | cut -d '.' -f 2)
		verStep_own=$(echo "$ownVersion" | cut -d '.' -f 2)
		# echo -e "minor\nverStep_src='$verStep_src'\nverStep_own='$verStep_own'\n"

		if [ "$verStep_own" -lt "$verStep_src" ]; then versionsEqual=false
		else
			# checking for build diffs
			verStep_src=$(echo "$src_version" | cut -d '.' -f 3)
			verStep_own=$(echo "$ownVersion" | cut -d '.' -f 3)
			# echo -e "build\nverStep_src='$verStep_src'\nverStep_own='$verStep_own'\n"

			if [ "$verStep_own" -lt "$verStep_src" ]; then versionsEqual=false
			else
				# checking for revision diffs
				verStep_src=$(echo "$src_version" | cut -d '.' -f 4)
				verStep_own=$(echo "$ownVersion" | cut -d '.' -f 4)
				# echo -e "revision\nverStep_src='$verStep_src'\nverStep_own='$verStep_own'\n"

				if [ "$verStep_own" -lt "$verStep_src" ]; then versionsEqual=false; fi
			fi
		fi
	fi
fi

echo -e "\tversionsEqual:\t'$versionsEqual'"

if [ "$versionsEqual" == false ]; then

	echo -e "\t... updating myself"

	self="./$scriptName"
	self_backup="$self"'_backup'

	# removing old backup
	if [ -f "$self_backup" ]; then rm -f "$self_backup"; fi
	mv "$self" "$self_backup"

	echo "$src_self" >> "$self"
	chmod +x "$self"

	"$self"
	
	sleep 1s && exit 1
fi

# continue with regular routine
latestSource=$(curl -sL "https://secondlife.com/support/downloads/")

latestLink=$(echo "$latestSource" | grep -v ".exe" | grep "$arch" | cut -d '"' -f 2)
echo -e "\n\tlatestLink:\t$latestLink"

latestVersion=$(basename "$latestLink" | cut -d '_' -f 3- | cut -d '.' -f 1 | tr '_' '.' | rev | cut -d '.' -f 2- | rev)
echo -e "\tlatestVersion:\t$latestVersion"
echo -e "\tcurrentVersion:\t$currentVersion"

versionsEqual=true
if [ "$latestVersion" != "$currentVersion" ]; then versionsEqual=false; fi
echo -e "\tversionsEqual:\t'$versionsEqual'"

if [ "$versionsEqual" == false ] || [ "$forcedUpdate" == true ]; then
	echo -e "\n[$appName updater] updating $currentName\n\tforcedUpdate:\t'$forcedUpdate'"
	echo -ne "\033]0;[:: $appName Updater ::] - Update available!\007"

	optionsArray=("TRUE" "UPDATE --> $latestLink")

	info="current version: $currentName $currentVersion | online: '$online' | forcedUpdate: '$forcedUpdate'"
	response=$(zenity --title="[:: $appName ::]" --width=810 --height=250 --list --radiolist --text="$info" \
--column="" --column="Please choose an option" "${optionsArray[@]}" 2>/dev/null)

	if [ ! -n "$response" ]; then
		echo -e "\tskipped updating.."

	else		
		# useRelease=""
		# if [ "$response" == "UPDATE --> $item" ]; then useRelease="$latestLink"; fi
		useRelease="$latestLink"
		echo -e "\tuseRelease:\t'$useRelease'"

		# prepare local .backups folder if not existing
		echo -ne "\033]0;[:: $appName Updater ::] - creating backup\007"
		if [ ! -d ".backups" ]; then echo -e "\tcreating folder '.backups'"; mkdir ".backups"; fi

		now=$(date +%d.%m.%Y_%H:%M:%S)
		backupFolder=".backups/backup_SecondLife_$currentVersion ($now)"

		if [ ! -d "$backupFolder" ]; then echo -e "\tcreating folder '$backupFolder'"; mkdir "$backupFolder"; fi

		# copy current SecondLife Viewer install files & folders
		# every backup contains 2 folders, the binary and the user settings folder in $HOME

		# copy $userSettingsDir to backup
		echo -e "\n\tcopy '$userSettingsDir' to '$backupFolder'"
		rsync -rhl --info=progress2 "$userSettingsDir" "$backupFolder"

		mkdir "$backupFolder/binary"

		# copy binary to backup
		copyList=($(find -mindepth 1 -maxdepth 1 -print | sort))

		for item in "${copyList[@]}"; do
			echo "item='$item'"

			if [ "$item" != "./.backups" ]; then
				echo -e "\n\tcopy '$item' to '$backupFolder/binary'"
				rsync -rhl --info=progress2 "$item" "$backupFolder/binary"
			fi
		done

		echo -e "\n\tremoving old binary files"
		echo -ne "\033]0;[:: $appName Updater ::] - removing old binary files\007"

		removeList=()
		removeList="${copyList[@]}"

		if [ ${#excludeItems[@]} -gt 0 ]; then
			ignParams=()

			echo -e "\n\tusing ignore list"

			for item in "${excludeItems[@]}"; do
				if [ -f "$item" ] || [ -d "$item" ]; then ignParams+=("!" "-name" "$item"); fi
			done

			echo -e "\n\tignParams: '${ignParams[@]}'"

			removeList=($(find -mindepth 1 -maxdepth 1 "${ignParams[@]}" -print | sort))
		fi

		for item in "${removeList[@]}"; do if [ -f "$item" ] || [ -d "$item" ]; then rm -rv "$item"; fi; done

		echo -ne "\033]0;[:: $appName Updater ::] - downloading & unpacking v$latestVersion\007"
		echo -e "\n[$appName updater] grabbing $latestVersion via wget\n\trelease link --> $useRelease"
		wget -q --show-progress "$useRelease"

		package=$(basename "$useRelease")
		echo -e "\n\tunpacking $package and updating to $latestVersion"

		if [ -f "$package" ]; then
			echo -ne "\033]0;[:: $appName Updater ::] - downloading & unpacking v$latestVersion\007\n"
			sleep 5s

			tar -vxf "$package" && rm "$package"
		fi

		moveFolder=$(echo "$package" | rev | cut -d '.' -f 3- | rev)

		echo -e "\n\tmoveFolder\t'$moveFolder'\n\tpackage\t\t'$package'"

		if [ -d "$moveFolder" ]; then
			echo -e "\n[$appName updater] .. moving new release files from '$moveFolder'"
			mv "./$moveFolder/"* . && rm -r "./$moveFolder"
		fi
	fi
fi

if [ -f "$buildFile" ]; then currentVersion=$(cat "$buildFile" | jq -r '.Version'); fi

echo -ne "\033]0;[:: $appName $currentVersion ::]\007"
echo -e "\n[$appName updater] executing $appName\n\tappCMD:\t'$appCMD'\n" && sleep 2s
"$appCMD"

exit

